# Non-fiducial flux

Study of the applicability of the non-fiducial events.

The electron analysis uses the non-fiducial events to increase the counts at high energies. Here we study if the same approach is valid for the ion analysis, in particular for the helium flux analysis. The non-fiducial events are those that don't pass the skim cuts. I.e. the events with shower not fully contained in the BGO calorimeter. These events can still have a reliable PSD signal, so the ions can be well identified. However, the ratio BGO/Kin energy can be much worse for the non-fiducial events, which makes the unfolding matrix highly non-diagonal (more than usually).